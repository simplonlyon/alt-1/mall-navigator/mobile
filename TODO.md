# __Mall navigator mobile__
 
 ## Consignes Clément Lundi matin :
 
 - Liste de lieux par coordonnées
  - Afficher boussole vers lieu + distance (+étage ?)


  ___Parcours user :___
 - Appli centre commerciaux
 - Avoir une app mobile permettant de trouver un magasin dans un centre commercial à la manière d'un air tag apple
 - Sans login
 - En tant qu'utilisateur, on me demande de
  1. Sélectionner une structure (géolocalisation)
     1. Cela affiche une liste de boutiques
     2. Cela permet de scanner un QR CODE
     - Affiche une flèche vers le magasin avec nombre de pas / nombre de mètres


Les services et entités (src/services/* & types/*) sont basées sur des données factices, à modifier au branchement de l'API.


__FIGMA: https://www.figma.com/file/5N9EpCulHvC86wnytoGIn3/Simplon-compass?node-id=0%3A1__

## Explications arborescence

 - __qrCode/__ => qrCode de test, contient l'uuid d'un des shops de test
 - __src/globals__ => propriétés globales facilements modifiables
 - __src/helpers__ => fichiers dédiés à la logique pour ne pas encombrer les template react
 - __src/pages__ & src/pages/components => composants react
 - __src/services__ => fonctions d'api
 - __types/__ => types correspondant aux entités telles qu'elles existent sur l'application. Les fichiers .d.ts sont globaux à tout le projet, il n'est pas nécessaire de les importer.