import React from "react";
import { StyleSheet } from "react-native";
import { QueryClient, QueryClientProvider } from "react-query";
import { NavigationContainer, StackActions } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { QrCode } from "./src/pages/QrCode";
import { MallList } from "./src/pages/MallList";
import { Icon } from "react-native-elements/dist/icons/Icon";
import { ShopList } from "./src/pages/ShopList";
import { Compass } from "./src/pages/Compass";
import { ShopDetails } from "./src/pages/ShopDetails";

export type RootStackParamList = {
  mallList: undefined,
  qrCode: undefined,
  shopList: { mall: Mall },
  shopDetails: { shop: Shop },
  compass: { shop: Shop }
}

const Stack = createNativeStackNavigator();

export default function App() {
  // Create a client
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="mallList">
          <Stack.Screen
            name="mallList"
            component={MallList}
            options={({ navigation }) => ({
              title: "Shopping mall",
              headerRight: () => (
                <Icon
                  name="camera-enhance"
                  onPress={() => {
                    navigation.navigate("qrCode");
                  }}
                ></Icon>
              ),
            })}
          ></Stack.Screen>
          <Stack.Screen
            name="qrCode"
            component={QrCode}
            options={({ navigation }) => ({
              title: "Scan qr code",
              headerRight: () => (
                <Icon
                  name="menu"
                  onPress={() => {
                    // TODO: Rediriger sur un historique
                    navigation.navigate("mallList");
                  }}
                />
              ),
            })}
          ></Stack.Screen>
          <Stack.Screen
            name="shopList"
            component={ShopList}
            options={{ title: "Shop list" }}
          ></Stack.Screen>
          <Stack.Screen
            name="compass"
            component={Compass}
            options={{ title: "Compass" }}
          ></Stack.Screen>
          <Stack.Screen
            name="shopDetails"
            component={ShopDetails}
            options={{ title: "Shop details" }}
          ></Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    </QueryClientProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
