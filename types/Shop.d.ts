type Shop = {
    uuid: string,
    mall_uuid: string,
    label: string,
    description: string
    location: LocationObjectMinimal
}