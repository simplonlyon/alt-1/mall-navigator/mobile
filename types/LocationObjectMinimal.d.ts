// Basé sur le type react native Location.LocationObject sans les données non-nécessaires à l'instant T

type LocationObjectMinimal = {
    coords: {
        longitude: number,
        latitude: number
    }
}