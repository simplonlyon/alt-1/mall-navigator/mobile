// TODO: Remplacer les fonctions ici par des appels d'API.
// TODO: SHOP_DATA => Données temporaires => A supprimer.

const SHOPS_DATA: Shop[] = [
  {
    uuid: "2cf6453a-ddea-4bd0-8c3c-16fbd7b66daa",
    mall_uuid: "f23b53f9-49c1-41e4-951a-9a6fa7782dbe",
    label: "Mall 1 - Boutique 1",
    description: "La première boutique de la liste",
    location: {coords: {latitude: 45.7345602, longitude: 4.7389948}}
  },
  {
    uuid: "90eec28b-30f7-467b-9446-b52710802b89",
    mall_uuid: "f23b53f9-49c1-41e4-951a-9a6fa7782dbe",
    label: "Mall 1 - Boutique 2",
    description:"La deuxième boutique de la liste",
    location: {coords: {latitude: 50.7516299, longitude: 9.8428135}}
  },
  {
    uuid: "1671251c-e723-4f41-b427-442a93058609",
    mall_uuid: "f423cced-075d-4993-b098-c31f1178d582",
    label: "Mall 2 - Boutique 1",
    description:"La troisieme boutique de la liste",
    location: {coords: {latitude: 45.9455615, longitude: 4.9568958}}
  },
  {
    uuid: "9aec909a-0102-4eea-ad44-558d66818dcf",
    mall_uuid: "f423cced-075d-4993-b098-c31f1178d582",
    label: "Chez Sam",
    description:"Une boutique super",
    location: {coords: {latitude: 45.7516282, longitude: 4.8428126}}
  },
  {
    uuid: "04114294-e08d-43ba-82ea-eee0a2ec46cd",
    mall_uuid: "f423cced-075d-4993-b098-c31f1178d582",
    label: "Mall 2 - Boutique 2",
    description:"La quatrième boutique de la liste",
    location: {coords: {latitude: 45.8525600, longitude: 4.6857950}}
  },
]


/**
 * Renvoie la liste des boutiques d'un centre commercial.
 * @returns Liste de boutiques
 */
 export const getShopListByMallUuid = (mallUuid: string): Promise<Shop[]> => {
  return new Promise((res) => {
    res(SHOPS_DATA.filter(mall => mall.mall_uuid === mallUuid));
  });
};
/**
 * Renvoie les details d'un shop
 * @returns renvoie les détails d'un magasin
 */
export const getShopDetails = (shopUuid: string): Promise<Shop> =>{
  return new Promise((res)=>{
    // filter renvoie un tableau avec l'item filtré.
    res(SHOPS_DATA.filter(shop => shop.uuid === shopUuid)[0]);
  });
}
