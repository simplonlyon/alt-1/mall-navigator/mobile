// TODO: Remplacer les fonctions ici par des appels d'API.

/**
 * Renvoie la liste des centres à proximité.
 * @returns Liste de centres
 */
export const getMallList = (): Promise<Mall[]> => {
  return new Promise((res) => {
    res([
      {
        uuid: "f23b53f9-49c1-41e4-951a-9a6fa7782dbe",
        label: "Centre commercial 1"
      },
      {
        uuid: "f423cced-075d-4993-b098-c31f1178d582",
        label: "Centre commercial 2"
      },
    ]);
  });
};
