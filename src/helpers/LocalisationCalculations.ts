
/**
 * Renvoie une représentation affichable de la distance avec son unité de mesure
 * @param lat1 
 * @param lon1 
 * @param lat2 
 * @param lon2 
 * @returns 
 */
export function getDistanceBeautified(lat1: number, lon1: number, lat2: number, lon2: number) {
    let distance = getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2);

    if (distance < 1) {
        return (Math.floor(distance * 100) / 100) / 1000 + "m"
    }
    else {
        return (Math.floor(distance * 100) / 100) + "km"
    }
}



/**
 * Calcules la distance entre deux points gps (x,y)
 * @param lat1 
 * @param lon1 
 * @param lat2 
 * @param lon2 
 * @returns 
 */
export function getDistanceFromLatLonInKm(lat1: number, lon1: number, lat2: number, lon2: number) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

/**
 * Change une valeur en degré à une valeur en radiants.
 * @param deg 
 * @returns 
 */
function deg2rad(deg: number) {
    return deg * (Math.PI / 180)
}


/**
 * NON FONCTIONNEL.
 * TODO: Des idées intéressantes dans un repaire orthonormé. Ne donne pour l'instant pas la bonne direction.
 * Code trouvé sur: https://stackoverflow.com/questions/9614109/how-to-calculate-an-angle-from-points
 * @param shopX 
 * @param shopY 
 * @param posX 
 * @param posY 
 * @returns 
 */
export function calcAngle(shopX: number, shopY: number, posX: number, posY: number) {
    var dy = posY - shopY;
    var dx = posX - shopX;
    var theta = Math.atan2(dy, dx); // range (-PI, PI]
    theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
    if (theta < 0) theta = 360 + theta; // range [0, 360)

    if (shopX > posX && posY > shopY) {
        return theta;
    }
    if (shopX > posX && posY < shopY) {
        return theta += 90;
    }
    if (shopX < posX && posY < shopY) {
    }
    if (shopX < posX && posY > shopY) {
        return theta += 270;
    }
    return 0;
}