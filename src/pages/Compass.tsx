
import React, { useState, } from "react";
import * as Location from 'expo-location';
import RequestGeolocalisation from "./components/RequestGeolocalisation"
import { Text, View, StyleSheet } from 'react-native';
import { Icon } from "react-native-elements";
import { TERTIARY_COLOR } from "../global/SharedColor";
import { RootStackParamList } from "../../App";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { calcAngle, getDistanceBeautified } from "../helpers/LocalisationCalculations";

type CompassProps = NativeStackScreenProps<RootStackParamList, 'compass'>;

export function Compass({ route }: CompassProps) {
    const [location, setLocation] = useState<Location.LocationObject | null>(null);

    const { shop } = route.params;
    const shopCoords = shop.location.coords;

    const onLocalisationSuccess = (localisation: Location.LocationObject) => {
        setLocation(localisation)
    }

    /**
     * Récupères la distance du téléphone vers les coordonées gps de la boutique.
     * @returns 
     */
    const getShopDistance = () => {
        // Le if est un necessary fix, comportement chelou
        if (location?.coords.latitude) {
            return getDistanceBeautified(shopCoords.latitude, shopCoords.longitude, location?.coords.latitude, location?.coords.longitude);
        }
    }

    /**
     * Récupères l'angle que devra donner la boussole pour arriver à destination.
     * @returns 
     */
    const getCompassAngle = () => {
        // Le if est un necessary fix, comportement chelou
        if (location?.coords.latitude) {
            return calcAngle(shopCoords.latitude, shopCoords.longitude, location?.coords.latitude, location?.coords.longitude);
        }
    }

    const getRotationTransformation = () => {
        return { transform: [{ rotateZ: getCompassAngle() + 'deg' }] }
    }

    return (
        <>
            {location === null ? <RequestGeolocalisation onLocalisationSuccess={onLocalisationSuccess} /> : (
                <View style={styles.mainContainer}>
                    <Text>Vous êtes en Lat. {location?.coords.latitude.toString()}, Long. {location?.coords.longitude.toString()} </Text>
                    <Text>Vous êtes à {getShopDistance()} de la boutique: {shop.label} </Text>
                    <View style={styles.compassContainer}>
                        <View style={styles.compass}>
                            <Icon color={TERTIARY_COLOR} name="navigation" size={100} style={getRotationTransformation()} />
                        </View>
                    </View>
                </View>
            )}
        </>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        display: "flex",
        flexDirection: "column",
    },
    compassContainer: {
        justifyContent: "center",
        alignItems: "center",
    },
    compass: {
        borderRadius: 100,
        borderColor: TERTIARY_COLOR,
        borderWidth: 5,
        overflow: 'hidden',
        width: 200,
        height: 200,
        justifyContent: "center",
        alignItems: "center",
    },
    qrCode: {
        backgroundColor: "red",
        width: "55%",
        height: "65%",
        borderRadius: 10
    },
})
