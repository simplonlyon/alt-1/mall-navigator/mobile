import { BarCodeEvent, BarCodeScannedCallback, BarCodeScanner } from "expo-barcode-scanner";
import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text } from "react-native";
import { ACCENT_COLOR } from "../../global/SharedColor";
/**
 * @switchPage -> correspond à la fonction de navigation.
 */
interface qrCodeCam {
  switchPage:(shopUuid:string)=>void
}

/**
 * @param switchPage correspond à la fonction de navigation de  la page @qrCode 
 * @returns render
 */
export function QrCodeCamera({switchPage} : qrCodeCam) {
  
  const [hasPermission, setHasPermission] = useState(false);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);
  /**
   * Effectue le changement de page lors du scan du qrCode.
   * @param element -> correspond aux datas du qrCode scanné.
   */
  const handleBarCodeScanned: BarCodeScannedCallback  = (element: BarCodeEvent) => {
    const { data } = element;
    setScanned(true);
    switchPage(data);
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }
  return (
    <BarCodeScanner
      onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
      style={styles.qrCode}
    />
  );
}
const styles = StyleSheet.create({
  qrCode: {
    backgroundColor: ACCENT_COLOR,
    width: "60%",
    height: "65%",
    borderRadius: 10,
  },
});
