import React, { useState, useEffect } from 'react';
import { Platform, Text, View, StyleSheet } from 'react-native';
import * as Location from 'expo-location';

interface RequestLocationProps {
    onLocalisationSuccess: (location: Location.LocationObject) => void,
    onLocalisationError?: (error: Error) => void,
}

function RequestGeolocalisation({ onLocalisationSuccess: onLocationSuccess, onLocalisationError: onLocationError }: RequestLocationProps) {
    const [location, setLocation] = useState<Location.LocationObject | null>(null);
    const [errorMsg, setErrorMsg] = useState<string | null>(null);

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }
            let location = await Location.getCurrentPositionAsync({});
            setLocation(location);
        })();
    }, []);

    let text = 'Waiting..';
    if (errorMsg) {
        text = errorMsg;
    } else if (location) {
        onLocationSuccess(location)
    }

    return (
        <View>
            <Text>{text}</Text>
        </View>
    );

}

export default RequestGeolocalisation;