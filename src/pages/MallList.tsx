import React from "react";
import { FlatList, StyleSheet, View, Text } from "react-native";
import { useQuery } from "react-query";
import { getMallList } from "../services/MallService";
import { Card, Icon } from "react-native-elements";
import { PRIMARY_COLOR, ACCENT_COLOR } from "../global/SharedColor";
import { ProgressBar } from 'react-native-paper';
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootStackParamList } from "../../App";

type Props = NativeStackScreenProps<RootStackParamList, 'mallList'>;

export function MallList({navigation}:Props) {
  // Queries
  const { status, data } = useQuery("mall", getMallList);

  const goToShopList = (mall: Mall) => {
    navigation.navigate('shopList', {
      mall: mall
    });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Liste des centre commerciaux à proximité</Text>
      {status == "loading" && (
        <ProgressBar>
        </ProgressBar>
      )}
      {status === "success" && (
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <>
              <Card>
                <Card.Title style={styles.listItem} onPress={() => goToShopList(item)}>
                  {item.label} <Icon color={ACCENT_COLOR} name="chevron-right" />
                </Card.Title>
              </Card>
            </>
          )}
          keyExtractor={(item) => item.uuid}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  listItem: {
    textAlign: "center",
    fontSize: 28,
    color: ACCENT_COLOR,
  },
  title: {
    fontSize: 35,
    color: PRIMARY_COLOR,
  },
});
