import React from "react";
import { FlatList, StyleSheet, View, Text } from "react-native";
import { useQuery, useQueryClient } from "react-query";
import { getShopListByMallUuid } from "../services/ShopService";
import { Card, Icon } from "react-native-elements";
import { PRIMARY_COLOR, ACCENT_COLOR } from "../global/SharedColor";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootStackParamList } from "../../App";
import { CommonActions } from "@react-navigation/native";
import { ProgressBar } from "react-native-paper";

type ShopListProps = NativeStackScreenProps<RootStackParamList, 'shopList'>; 

export function ShopList({ route, navigation }: ShopListProps) {

  const { mall } = route.params;

  if (!mall) {
    navigation.dispatch(CommonActions.goBack());
  }

  // Queries
  const {status, data} = useQuery("shop", () => getShopListByMallUuid(mall.uuid));
  /**
   * Permet d'accèder au détails du shop en cliquant dessus.
   * @param shop 
   */
  const goToShop = (shop: Shop) => {
    navigation.navigate('shopDetails', {
      shop: shop
    });
  };

  return (
    <View>
      <Text style={styles.title}>Liste des centre commerciaux à proximité</Text>
      {status == "loading" && <ProgressBar></ProgressBar>}
      {status === "success" && data && (
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <>
              <Card>
                <Card.Title style={styles.listItem} onPress={() => goToShop(item)}>
                  {item.label}{" "}
                  <Icon color={ACCENT_COLOR} name="chevron-right" />
                </Card.Title>
              </Card>
            </>
          )}
          keyExtractor={(item) => item.uuid}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  listItem: {
    textAlign: "center",
    fontSize: 28,
    color: ACCENT_COLOR,
  },
  title: {
    fontSize: 35,
    color: PRIMARY_COLOR,
  },
});
