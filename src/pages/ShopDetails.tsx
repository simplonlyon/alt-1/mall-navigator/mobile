import React from "react";
import { View, StyleSheet, Text, Button } from "react-native";
import { useQuery } from "react-query";
import { getShopDetails } from "../services/ShopService";
import { ProgressBar } from 'react-native-paper';
import { RootStackParamList } from "../../App";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { ACCENT_COLOR } from "../global/SharedColor";

type Props = NativeStackScreenProps<RootStackParamList, 'shopDetails'>;

export function ShopDetails({ route, navigation }: Props) {

    const shop: Shop = route.params.shop;

    // TODO: checker si shop est un objet complet.

    const goToCompass = (): void => {
        navigation.navigate("compass", {
            shop: shop
        });
    };

    return (
        <View style={styles.container}>
            <>
                <View style={styles.container}>
                    <View style={styles.titleContainer}>
                        <Text style={{ fontSize: 18, fontWeight: "bold" }}>{shop.label}</Text>
                    </View>
                    <View style={styles.descContainer}>
                        <Text style={styles.descTitle}> Description :</Text>
                        <Text style={styles.description}>{shop.description}</Text>
                    </View>
                    <Button
                        onPress={goToCompass}
                        title={"EN ROUTE !"}
                    ></Button>
                </View>
            </>
        </View>
    );
}
const styles = StyleSheet.create({
    descContainer: {
        flex: 2,
        width: "100%",
        margin: 12,
        alignItems: "flex-start",
    },
    descTitle: {
        fontSize: 23,
        fontWeight: "bold",
    },
    description: {
        fontSize: 16,
        marginLeft: 12
    },
    titleContainer: {
        width: "100%",
        flex: 1,
        backgroundColor: ACCENT_COLOR,
        alignItems: "center",
        justifyContent: "center"
    },
    container: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between"
    }
});