import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { useQuery } from "react-query";
import { RootStackParamList } from "../../App";
import { getShopDetails } from "../services/ShopService";
import { QrCodeCamera } from "./components/QrCodeCamera";

type Props = NativeStackScreenProps<RootStackParamList, 'qrCode'>;

export function QrCode({navigation}:Props){
    /**
     * Renvoie sur shopDetails avec l'uuid du qrCode scanné.
     * @return void
     */
    const goToDetailsPage = (shopUuid:string): void =>{
        // TODO: Checker les cas de status différents.
        // TODO: on envoie la méthode goToDetailsPage au composant QrCodeCamera, cela génère un throw car on ne peut pas déclencher de l'asynchrone dans un composant enfant => trouver un fix.
        const {status, data} = useQuery("shopDetails", () => getShopDetails(shopUuid));
        navigation.navigate("shopDetails", {
            shop:data!
        });
    }
    return (
        <View style={styles.mainContainer}>
            <View style={styles.qrCodeContainer}>
                <QrCodeCamera switchPage={goToDetailsPage}></QrCodeCamera>
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.textElement}>Scan du qrCode : </Text>
                <Text style={styles.subtitle}>Redirection auto</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    mainContainer:{
        display:"flex",
        flexDirection:"column",
    },
    qrCodeContainer:{
        justifyContent:"center",
        alignItems: "center",
        height:"50%",
    },
    qrCode:{
        backgroundColor:"red", 
        width: "55%", 
        height:"65%",
        borderRadius:10
    },
    textContainer:{
        width:"100%", 
        height:"50%",
        alignItems:"center",
        justifyContent:"center"
    },
    textElement:{
        fontWeight:"bold",
        fontSize: 20,
        marginBottom:10
    },
    subtitle:{
        fontSize: 15
    }
});